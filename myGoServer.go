package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os/exec"
)

type Identity struct {
	InstanceId       string
	InstanceType     string
	PrivateIp        string
	AvailabilityZone string
	RespStr          string
}

var script string
var identity Identity

func exec_load(cpuLoadTime string, latency string) error {

	if _, err := exec.Command("bash", "-c", script, "", cpuLoadTime, latency).Output(); err == nil {
		identity.RespStr = "Executed script to load server CPU - " + cpuLoadTime + " and introduce latency - " + latency
		return nil
	} else {
		log.Println("Unable to execute bash script to load CPU")
		return errors.New("Unable to execute bash script to load CPU")
	}

	return nil

}

func serveResp(w http.ResponseWriter, r *http.Request) {

	var cpuLoadTime, latency string

	if q, err := url.ParseQuery(r.URL.RawQuery); err == nil {

		if _, ok := q["cpuLoadTime"]; !ok {
			http.Error(w, "Request failed. Check query strings on the request \n", 400)
			return
		} else {
			cpuLoadTime = q["cpuLoadTime"][0]
		}

		if _, ok := q["latency"]; !ok {
			http.Error(w, "Request failed. Check query strings on the request \n", 400)
			return
		} else {
			latency = q["latency"][0]
		}

		exec_load(cpuLoadTime, latency)
		fmt.Fprintf(w, "%+v", identity)
		fmt.Fprintf(w, "\n")
	} else {
		http.Error(w, "Request failed. Check query strings on the request \n", 400)
	}

}

func main() {
	if bashcmds, err := ioutil.ReadFile("./loadCPU.sh"); err != nil {
		log.Fatal(err)
	} else {
		script = string(bashcmds)
	}

	getInstanceDets()
	http.HandleFunc("/", serveResp)
	http.ListenAndServe(":8889", nil)
}

func getInstanceDets() {
	if res, err := http.Get("http://169.254.169.254/latest/dynamic/instance-identity/document"); err == nil {
		if res.StatusCode == 200 {
			if bytStr, err := ioutil.ReadAll(res.Body); err == nil {
				json.Unmarshal(bytStr, &identity)
				fmt.Println("%s", identity)
				res.Body.Close()
			} else {
				log.Println(err)
			}
		} else {
			log.Println(res)
		}
	} else {
		log.Println(err)
	}
}

/*
func getInstanceDets() {
	res, _ := ioutil.ReadFile("jsonData.json")
	json.Unmarshal(res, &identity)
	fmt.Println(identity)
}
*/
