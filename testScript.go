package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type Config struct {
	Random      string
	CpuLoadTime int
	Latency     int
	Hpm         int
	FullURL     string
	Port        int
}

var config Config

func pingServer() (robots []byte, err error) {
	var urlForm string
	if config.Random == "y" {
		cpuLoadTime, latency := random_load()
		urlForm = config.FullURL + ":" +
			strconv.Itoa(config.Port) +
			"?cpuLoadTime=" + cpuLoadTime +
			"&latency=" + latency
	} else {
		urlForm = config.FullURL + ":" +
			strconv.Itoa(config.Port) +
			"?cpuLoadTime=" +
			strconv.Itoa(config.CpuLoadTime) +
			"&latency=" +
			strconv.Itoa(config.Latency)
	}

	if res, err := http.Get(urlForm); err == nil {
		if robots, err := ioutil.ReadAll(res.Body); err == nil {
			res.Body.Close()
			return robots, nil
		} else {
			log.Fatal(err)
			return nil, err
		}
	} else {
		log.Fatal(err)
		return nil, err
	}
	return robots, err
}

func random_load() (cpuLoadTime, latency string) {

	randNo := rand.New(rand.NewSource(time.Now().UnixNano()))
	r := randNo.Intn(5)

	switch r {
	case 1:
		cpuLoadTime = ""
		latency = "2s"
	case 2:
		cpuLoadTime = "2s"
		latency = ""
	case 3:
		cpuLoadTime = "2s"
		latency = "3s"
	case 4:
		cpuLoadTime = "5s"
		latency = ""
	case 5:
		cpuLoadTime = "5s"
		latency = "5s"
	default:
		cpuLoadTime = ""
		latency = ""
	}

	return
}

func main() {
	config = readConfig()

	interval := 60 / config.Hpm

	//errCond := make(chan error, 100)

	for i := 1; i <= config.Hpm; i++ {
		go func() {
			if resp, err := pingServer(); err == nil {
				fmt.Printf("%s", resp)
			} else {
				//				errCond <- err
			}
			return
		}()

		//		if err := <-errCond; err != nil {
		//			break
		//		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}

func readConfig() (config Config) {

	if byteStr, err := ioutil.ReadFile("testScriptConfig.json"); err == nil {
		if err = json.Unmarshal(byteStr, &config); err == nil {
			return
		} else {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	return
}
